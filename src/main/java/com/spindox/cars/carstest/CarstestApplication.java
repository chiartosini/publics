package com.spindox.cars.carstest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarstestApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarstestApplication.class, args);
    }
}
