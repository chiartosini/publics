package com.spindox.cars.carstest.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDTO {

    @ApiModelProperty(notes = "Id/matricola univoco della macchina")
    private Long id;

    @ApiModelProperty(notes = "Brand della macchina")
    private String brand;

    @ApiModelProperty(notes = "Allestimento della macchina")
    private String fitting;

    @ApiModelProperty(notes = "Modello della macchina")
    private String model;

}
