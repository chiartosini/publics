package com.spindox.cars.carstest.controller;

import com.spindox.cars.carstest.dto.CarDTO;
import com.spindox.cars.carstest.entity.Car;
import com.spindox.cars.carstest.service.CarService;
import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "CarController", tags = "Controller operazioni CRUD per gestione Macchine")
public class CarController {

    private static final Logger LOGGER = LogManager.getLogger(CarController.class);
    @Autowired
    private CarService carService;

    /**
     * @param model of the car to search
     * @return the List of the cars found
     */
    @ApiOperation(
            value = "Ricerca le macchine in base al modello",
            notes = "Restituisce lo stato della richiesta",
            response = List.class,
            produces = "application/json")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "macchina trovate"),
                    @ApiResponse(code = 204, message = "macchine non trovate")
            })
    @GetMapping("/car/model/{model}")
    public ResponseEntity<List<Car>> getByModel(@ApiParam("Modello della macchina") @PathVariable String model) {
        LOGGER.debug("getByModel Start with model {}", model);
        List<Car> cars = carService.getByModel(model);

        LOGGER.debug("getByModel Ended with Cars {}", cars);

        if (cars.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } else {
            return new ResponseEntity<>(cars, HttpStatus.OK);

        }
    }

    /**
     * @param id of the car to search
     * @return the retrieved car
     */
    @ApiOperation(
            value = "Ricerca una macchina",
            notes = "Restituisce lo stato della richiesta",
            response = Car.class,
            produces = "application/json")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "macchina trovata"),
                    @ApiResponse(code = 204, message = "macchina non trovata")
            })
    @GetMapping("/car/{id}")
    public ResponseEntity<Car> getCar(@ApiParam("Id univoco della macchina") @PathVariable Long id) {
        LOGGER.debug("getById Start with id {}", id);

        Car car = carService.getById(id);

        LOGGER.debug("getById Ended with car {}", car);

        if (car != null) {
            return new ResponseEntity<>(car, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * @param id of the car to delete
     * @return the object of the deleted car
     */
    @ApiOperation(
            value = "Cancella una macchina",
            notes = "Restituisce lo stato della richiesta",
            response = String.class,
            produces = "application/json")
    @ApiResponses(value = {@ApiResponse(code = 204, message = "macchina non creata")})
    @DeleteMapping("/car/{id}")
    public ResponseEntity<String> deleteCarBy(@ApiParam("Id univoco della macchina") @PathVariable Long id) {
        LOGGER.debug("deleteCarById Start with id {}", id);

        Car car = carService.deleteCarById(id);

        LOGGER.debug("deleteCarById Ended with car {}", car);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /**
     * @param newCar the new car object to create
     * @return the car object saved
     */
    @ApiOperation(
            value = "Crea una nuova macchina",
            notes = "Restituisce lo stato della richiesta",
            response = String.class,
            produces = "application/json")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "macchina creata"),
                    @ApiResponse(code = 204, message = "macchina non creata")
            })
    @PostMapping("/car")
    public ResponseEntity<String> createCar(@RequestBody CarDTO newCar) {
        LOGGER.debug("createCar Start with car {}", newCar);

        Car car = carService.createCar(newCar);

        LOGGER.debug("createCar Ended with car {}", car);

        if (car != null) {
            return new ResponseEntity<>("Success", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Car not created", HttpStatus.NO_CONTENT);
        }
    }

    /**
     * @param newCar the new car object to ue for update
     * @param id     of the old car to update
     * @return the car object saved
     */
    @ApiOperation(
            value = "Update delle macchine",
            notes = "Restituisce lo stato della richiesta",
            response = String.class,
            produces = "application/json")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "macchina aggiornata"),
                    @ApiResponse(code = 201, message = "macchina creata"),
                    @ApiResponse(code = 204, message = "macchina non aggiornata o creata")
            })
    @PutMapping("/car/{id}")
    public ResponseEntity<String> updateCar(@ApiParam("Id univoco della macchina") @RequestBody CarDTO newCar, @PathVariable Long id) {
        LOGGER.debug("updateCar Start with car {} and id {}", newCar, id);

        Car car = carService.updateCar(newCar, id);

        LOGGER.debug("updateCar Ended with car {}", car);

        if (car != null) {
            if(car.getId() != null && car.getId() != id){
                return new ResponseEntity<>("Success", HttpStatus.CREATED);
            }else{
                return new ResponseEntity<>("Success", HttpStatus.OK);
            }

        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    /**
     * @return the list of all cars
     */
    @ApiOperation(
            value = "Ricerca tutti le macchine",
            notes = "Restituisce la lista delle macchine in formato JSON",
            response = List.class,
            produces = "application/json")
    @ApiResponses(value =
            {@ApiResponse(code = 200, message = "macchina/e sono stati trovati"),
                    @ApiResponse(code = 204, message = "Non è stato trovato alcuna macchina")
            })
    @GetMapping("/car")
    public ResponseEntity<List<Car>> getCars() {
        LOGGER.debug("getCars Start");

        List<Car> cars = carService.getAllCars();

        LOGGER.debug("getById Ended");

        if (cars.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } else {
            return new ResponseEntity<>(cars, HttpStatus.OK);

        }
    }
}
