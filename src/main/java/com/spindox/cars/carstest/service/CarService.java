package com.spindox.cars.carstest.service;

import com.spindox.cars.carstest.dto.CarDTO;
import com.spindox.cars.carstest.entity.Car;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CarService {

    List<Car> getByModel(String model);

    Car getById(Long id);

    Car deleteCarById(Long id);

    Car createCar(CarDTO car);

    Car updateCar(CarDTO newCar, Long id);

    public List<Car> getAllCars();
}
