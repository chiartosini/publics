package com.spindox.cars.carstest.service.impl;

import com.spindox.cars.carstest.dto.CarDTO;
import com.spindox.cars.carstest.entity.Car;
import com.spindox.cars.carstest.exception.ResourceNotFoundException;
import com.spindox.cars.carstest.repository.CarRepository;
import com.spindox.cars.carstest.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    CarRepository carRepository;

    @Override
    public List<Car> getByModel(String model) {
        return carRepository.findByModel(model);
    }

    @Override
    public Car getById(Long id) {

        return carRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Car with id = " + id));
    }

    public Car deleteCarById(Long id) {
        Car car = carRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Not found Car with id = " + id));

        carRepository.delete(car);
        return car;
    }

    public List<Car> getAllCars() {
        return carRepository.findAll();

    }

    public Car createCar(CarDTO newCarDTO) {

        Car newCar = new Car();
        newCar.setBrand(newCarDTO.getBrand());
        newCar.setModel(newCarDTO.getModel());
        newCar.setFitting(newCarDTO.getFitting());

        return carRepository.save(newCar);
    }

    public Car updateCar(CarDTO newCarDTO, Long id) {

        Car newCar = new Car();
        newCar.setBrand(newCarDTO.getBrand());
        newCar.setModel(newCarDTO.getModel());
        newCar.setFitting(newCarDTO.getFitting());

        Car oldCar = carRepository.findById(id).orElseGet(() -> carRepository.save(newCar));

        if (newCar.getFitting() != null) {
            oldCar.setFitting(newCar.getFitting());
        }
        if (newCar.getModel() != null) {
            oldCar.setModel(newCar.getModel());
        }
        if (newCar.getBrand() != null) {
            oldCar.setBrand(newCar.getBrand());
        }

        return carRepository.save(oldCar);
    }

}
