package com.spindox.cars.carstest.service;

import com.spindox.cars.carstest.dto.CarDTO;
import com.spindox.cars.carstest.entity.Car;
import com.spindox.cars.carstest.exception.ResourceNotFoundException;
import com.spindox.cars.carstest.repository.CarRepository;
import com.spindox.cars.carstest.service.impl.CarServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    @Mock
    private CarRepository carRepository;

    @InjectMocks
    private CarServiceImpl carService;

    @Test
    public void whenSaveCar_shouldReturnCar() {
        Car car = new Car();
        car.setModel("Test Model");
        car.setFitting("Test Fitting");
        car.setBrand("Test Brand");

        when(carRepository.save(ArgumentMatchers.any(Car.class))).thenReturn(car);

        CarDTO carDTO = new CarDTO();
        carDTO.setModel("Test Model");
        carDTO.setFitting("Test Fitting");
        carDTO.setBrand("Test Brand");

        Car created = carService.createCar(carDTO);

        assertEquals(car.getBrand(), created.getBrand());
        verify(carRepository).save(car);
    }

    @Test
    public void whenGivenId_shouldDeleteCar_ifFound() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));

        carService.deleteCarById(car.getId());
        verify(carRepository).delete(car);
    }

    @Test(expected = RuntimeException.class)
    public void should_throw_exception_when_car_doesnt_exist() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        given(carRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        carService.deleteCarById(car.getId());
    }

    @Test
    public void shouldReturnAllCars() {
        List<Car> cars = new ArrayList();
        cars.add(new Car());

        given(carRepository.findAll()).willReturn(cars);

        List<Car> expected = carService.getAllCars();

        assertEquals(expected, cars);
        verify(carRepository).findAll();
    }

    @Test
    public void whenGivenId_shouldUpdateCar_ifFound() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        CarDTO carDTO = new CarDTO();
        carDTO.setModel("New Test Model");
        carDTO.setFitting("Test Fitting");
        carDTO.setBrand("Test Brand");

        Car newCar = new Car();
        newCar.setBrand("Test Brand");
        newCar.setFitting("Test Fitting");
        newCar.setModel("New Test Model");
        newCar.setId(89L);

        given(carRepository.findById(car.getId())).willReturn(Optional.of(car));
        carService.updateCar(carDTO, car.getId());

        verify(carRepository).save(newCar);
        verify(carRepository).findById(car.getId());
    }

    @Test(expected = RuntimeException.class)
    public void should_throw_exception_when_update_car_doesnt_exist() {
        Car car = new Car();
        car.setId(89L);
        car.setFitting("Test Fitting");

        CarDTO carDTO = new CarDTO();
        carDTO.setModel("Test Model");
        carDTO.setFitting("New Test Fitting");
        carDTO.setBrand("Test Brand");

        Car newCar = new Car();
        newCar.setId(90L);
        car.setFitting("New Test Fitting");

        given(carRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        carService.updateCar(carDTO, car.getId());
    }

    @Test
    public void whenGivenId_shouldReturnCar_ifFound() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));

        Car expected = carService.getById(car.getId());

        assertThat(expected).isSameAs(car);
        verify(carRepository).findById(car.getId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void should_throw_exception_when_get_car_doesnt_exist() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        given(carRepository.findById(anyLong())).willReturn(Optional.ofNullable(null));
        carService.getById(car.getId());
    }

    @Test
    public void whenGivenModel_shouldReturnCar_ifFound() {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        List<Car> cars = new ArrayList();
        cars.add(new Car());

        when(carRepository.findByModel(car.getModel())).thenReturn(cars);

        List<Car> expected = carService.getByModel(car.getModel());

        assertEquals(expected, cars);
        verify(carRepository).findByModel(car.getModel());

    }

}
