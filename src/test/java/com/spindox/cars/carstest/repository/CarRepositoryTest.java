package com.spindox.cars.carstest.repository;

import com.spindox.cars.carstest.CarstestApplication;
import com.spindox.cars.carstest.entity.Car;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = CarstestApplication.class)
class CarRepositoryTest {

    Car car;
    Car newCar;
    @MockBean
    private CarRepository carRepository;

    @BeforeEach
    void setup() {
        car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(1L);

        newCar = new Car();
        newCar.setBrand("Test Brand");
        newCar.setFitting("New Test Fitting");
        newCar.setModel("Test Model");
        newCar.setId(1L);
    }

    @Test
    void testFindById() {
        when(carRepository.findById(1L)).thenReturn(Optional.of(car));

        assertThat(carRepository.findById(1L).get())
                .extracting(Car::getFitting)
                .isEqualTo("Test Fitting");
    }

    @Test
    void testAddCar() {

        when(carRepository.save(car)).thenReturn(car);

        carRepository.save(car);

        when(carRepository.findById(1L)).thenReturn(Optional.of(car));

        assertThat(carRepository.findById(1L).get())
                .extracting(Car::getFitting)
                .isEqualTo("Test Fitting");
    }

    @Test
    void testUpdateCar() {

        when(carRepository.save(newCar)).thenReturn(newCar);

        carRepository.save(newCar);

        when(carRepository.findById(1L)).thenReturn(Optional.of(newCar));

        assertThat(carRepository.findById(newCar.getId()).get())
                .extracting(Car::getFitting)
                .isEqualTo("New Test Fitting");
    }

    @Test
    void testDeleteCar() {
        carRepository.delete(car);
        verify(carRepository).delete(car);
    }
}
