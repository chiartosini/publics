package com.spindox.cars.carstest.controller;

import com.spindox.cars.carstest.CarstestApplication;
import com.spindox.cars.carstest.MessageConfig;
import com.spindox.cars.carstest.dto.CarDTO;
import com.spindox.cars.carstest.entity.Car;
import com.spindox.cars.carstest.exception.ResourceNotFoundException;
import com.spindox.cars.carstest.service.impl.CarServiceImpl;
import com.spindox.cars.carstest.util.JsonUtil;
import org.json.JSONException;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CarController.class)
@Import(MessageConfig.class)
@ContextConfiguration(classes = CarstestApplication.class)
public class CarControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarServiceImpl service;

    @BeforeEach
    public void setup() throws JSONException, IOException {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
    }

    @Test
    public void createCar_whenPostMethod() throws Exception {

        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");


        CarDTO carDTO = new CarDTO();
        carDTO.setBrand("Test Brand");
        carDTO.setFitting("Test Fitting");
        carDTO.setModel("Test Model");

        given(service.createCar(carDTO)).willReturn(car);

        mockMvc.perform(post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtil.toJson(car)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$", is("Success")));
    }

    @Test
    public void removeCarById_whenDeleteMethod() throws Exception {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        given(service.deleteCarById(car.getId())).willReturn(car);

        mvc.perform(delete("/car/" + car.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void should_throw_exception_when_car_doesnt_exist() throws Exception {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        Mockito.doThrow(new ResourceNotFoundException(car.getId().toString())).when(service).deleteCarById(car.getId());

        mvc.perform(delete("/car/" + car.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void updateCar_whenPutCar() throws Exception {

        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        CarDTO carDTO = new CarDTO();
        carDTO.setBrand("Test Brand");
        carDTO.setFitting("Test Fitting");
        carDTO.setModel("Test Model");

        given(service.updateCar(carDTO, car.getId())).willReturn(car);

        mvc.perform(put("/car/" + car.getId().toString())
                        .content(JsonUtil.toJson(carDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Success")));
    }

    @Test
    public void updateCar_whenIdDontExist() throws Exception {

        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        CarDTO carDTO = new CarDTO();
        carDTO.setBrand("Test Brand");
        carDTO.setFitting("Test Fitting");
        carDTO.setModel("Test Model");

        given(service.updateCar(carDTO, 88L)).willReturn(car);

        mvc.perform(put("/car/" + 88)
                        .content(JsonUtil.toJson(carDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Success")));
    }

    @Test
    public void listAllCars_whenGetMethod()
            throws Exception {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);
        List<Car> allCars = Arrays.asList(car);

        given(service
                .getAllCars())
                .willReturn(allCars);

        mvc.perform(get("/car")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].fitting", is(car.getFitting())));
    }

    @Test
    public void CarById_whenGetMethod() throws Exception {

        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        given(service.getById(car.getId())).willReturn(car);

        mvc.perform(get("/car/" + car.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("model", is(car.getModel())));
    }

    @Test
    public void should_throw_exception_when_get_car_doesnt_exist() throws Exception {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);

        Mockito.doThrow(new ResourceNotFoundException(car.getId().toString())).when(service).getById(car.getId());

        mvc.perform(get("/car/" + car.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void listAllCarsByModel_whenGetMethod() throws Exception {
        Car car = new Car();
        car.setBrand("Test Brand");
        car.setFitting("Test Fitting");
        car.setModel("Test Model");
        car.setId(89L);
        List<Car> allCars = Arrays.asList(car);

        given(service.getByModel(car.getModel())).willReturn(allCars);

        mvc.perform(get("/car/model/" + car.getModel())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].model", is(car.getModel())));
    }
}