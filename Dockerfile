FROM openjdk:8-jdk-alpine
USER root
COPY target/carstest-0.0.1-SNAPSHOT/WEB-INF/lib /app/lib
COPY target/carstest-0.0.1-SNAPSHOT/META-INF /app/META-INF
COPY target/carstest-0.0.1-SNAPSHOT/WEB-INF/classes /app
COPY target/carstest-0.0.1-SNAPSHOT/WEB-INF/classes/log4j2.xml /src/main/resources/
COPY logs /logs
ENTRYPOINT ["java","-cp","app:app/lib/*","com.spindox.cars.carstest.CarstestApplication"]