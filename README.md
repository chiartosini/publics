# The project
The Application is a example of rest-full microservices that has been integrated with docker for containerized startup

## Startup
Install Docker and WSDL2 kernel

Build the project using Maven clean-install

Open the terminal in your project root

Build the DockerFile for create the image of your application using:
```
$ docker build -t spring-app .
```

Build and start the DB container and app using the docker-compose.yml:

User the first for build from local and the second for build from hub
```
$ docker-compose -f docker-local-compose.yml up -d --build
```
or
```
$ docker-compose -f docker-hub-compose.yml up -d --build
```

## Testing
The Application can be tested using Junit or importing the POSTMAN collection in the root of project,
remember to start using CREATE query, because the DB is empty and not pre-filled.

